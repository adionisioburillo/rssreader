package com.adionisio.rssreader.api;

import com.adionisio.rssreader.model.RssResponse;
import com.adionisio.rssreader.utils.Config;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Alejandro on 30/07/2018.
 */

public class RssServiceImpl {
    private RssServiceApi serviceApi;

    public RssServiceImpl() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(Config.RSSSERVER)
                .build();

        serviceApi = retrofit.create(RssServiceApi.class);
    }

    public Observable<RssResponse> getRssArticles(String source) {
        return serviceApi.getNews(Config.APIKEY, mapSource(source));
    }

    private String mapSource(String source) {
        switch (source) {
            case "Metro":
                source = "metro";
                break;
            case "ABC":
                source = "abc-news";
                break;
            case "BBC":
                source = "bbc-news";
                break;
            case "CNN":
                source = "cnn";
                break;
            case "The guardian":
                source = "the-guardian-uk";
                break;
            case "New York Times":
                source = "the-new-york-times";
                break;
        }
        return source;
    }
}
