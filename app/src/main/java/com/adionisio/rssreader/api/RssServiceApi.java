package com.adionisio.rssreader.api;

import com.adionisio.rssreader.model.RssResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Alejandro on 30/07/2018.
 */

public interface RssServiceApi {

    @GET("/v2/top-headlines")
    Observable<RssResponse> getNews(
            @Query("apiKey") String apiKey,
            @Query("sources") String sources
    );

}
