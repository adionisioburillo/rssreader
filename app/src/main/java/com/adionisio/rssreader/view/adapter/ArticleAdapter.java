package com.adionisio.rssreader.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.adionisio.rssreader.R;
import com.adionisio.rssreader.model.Article;
import com.adionisio.rssreader.view.listener.RecyclerViewOnClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alejandro on 30/07/2018.
 */

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder> implements Filterable {
    private ArrayList<Article> articles;
    private ArrayList<Article> articlesFiltered;
    private Context context;
    private RecyclerViewOnClickListener listener;

    public ArticleAdapter(Context context, ArrayList<Article> articles, RecyclerViewOnClickListener listener)

    {
        this.context = context;
        this.articles = articles;
        this.articlesFiltered = articles;
        this.listener = listener;

    }

    public Article getArticleByPosition(int position) {
        return articlesFiltered.get(position);
    }

    @Override
    public ArticleAdapter.ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ArticleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_article, parent, false), listener);
    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, int position) {
        Article article = articlesFiltered.get(position);
        holder.title.setText(article.getTitle());
        holder.description.setText(article.getDescription());
        Picasso.with(context).load(article.getUrlToImage()).fit().centerCrop().into(holder.image);
    }

    @Override
    public int getItemCount() {
        return articlesFiltered.size();
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String sequence = constraint.toString();

                if (sequence.isEmpty())
                    articlesFiltered = articles;
                else {
                    ArrayList<Article> articlesListTemp = new ArrayList();
                    for (Article article : articles) {
                        if (article.getTitle().toLowerCase().contains(sequence.toLowerCase()))
                            articlesListTemp.add(article);
                    }
                    articlesFiltered = articlesListTemp;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = articlesFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                articlesFiltered = (ArrayList<Article>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }


    public class ArticleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.textViewTitle)
        TextView title;
        @BindView(R.id.textViewDescription)
        TextView description;
        @BindView(R.id.imageViewArticle)
        ImageView image;
        private RecyclerViewOnClickListener listener;

        public ArticleViewHolder(View itemView, RecyclerViewOnClickListener listener) {
            super(itemView);
            this.listener = listener;
            itemView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(v, getAdapterPosition());
        }
    }
}
