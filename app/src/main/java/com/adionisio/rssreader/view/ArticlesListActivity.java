package com.adionisio.rssreader.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.adionisio.rssreader.R;
import com.adionisio.rssreader.model.Article;
import com.adionisio.rssreader.presenter.ArticlesListActivityPresenter;
import com.adionisio.rssreader.presenter.ArticlesListActivityPresenterImpl;
import com.adionisio.rssreader.view.adapter.ArticleAdapter;
import com.adionisio.rssreader.view.listener.RecyclerViewOnClickListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArticlesListActivity extends AppCompatActivity implements ArticlesListActivityPresenter.View, AdapterView.OnItemSelectedListener {

    public static final String ARTICLE = "article_selected";
    private static final String SPINNER_SELECTED = "source_selected";
    @BindView(R.id.layoutLoading)
    LinearLayout layoutLoading;
    @BindView(R.id.rv_articles)
    RecyclerView recyclerView;
    @BindView(R.id.spinner)
    Spinner spinner;
    ArticleAdapter articleAdapter;
    ArticlesListActivityPresenterImpl presenter;
    private RecyclerViewOnClickListener listener;
    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        setContentView(R.layout.activity_articles_list);
        ButterKnife.bind(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        listener = new RecyclerViewOnClickListener() {
            @Override
            public void onClick(View view, int position) {
                articleSelected(position);
            }
        };
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedPref = getPreferences(Context.MODE_PRIVATE);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.sources_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        presenter = new ArticlesListActivityPresenterImpl();
        presenter.setView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        spinner.setSelection(sharedPref.getInt(SPINNER_SELECTED, 0));
        presenter.init(spinner.getSelectedItem().toString());
    }

    private void articleSelected(int position) {
        Intent intent = new Intent(this.getApplicationContext(), ArticleDetailsActivity.class);
        Article article = articleAdapter.getArticleByPosition(position);
        intent.putExtra(ARTICLE, article);
        startActivity(intent);
    }

    @Override
    public void showLoading() {
        layoutLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        layoutLoading.setVisibility(View.GONE);
    }

    @Override
    public void sendFeedback(String title) {
        Snackbar.make(findViewById(android.R.id.content), title, Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void showArticles(ArrayList<Article> articlesList) {
        articleAdapter = new ArticleAdapter(this.getApplicationContext(), articlesList, listener);
        recyclerView.setAdapter(articleAdapter);
    }

    @Override
    public Context getContext() {
        return getBaseContext();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_list, menu);
        MenuItem search = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (articleAdapter != null)
                    articleAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        sharedPref.edit().putInt(SPINNER_SELECTED, spinner.getSelectedItemPosition()).commit();
        presenter.loadDataFromNewSource(spinner.getSelectedItem().toString());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}
