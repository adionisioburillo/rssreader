package com.adionisio.rssreader.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.adionisio.rssreader.R;
import com.adionisio.rssreader.presenter.ArticleDetailsPresenter;
import com.adionisio.rssreader.presenter.ArticleDetailsPresenterImpl;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ArticleDetailsActivity extends AppCompatActivity implements ArticleDetailsPresenter.View {

    private ArticleDetailsPresenter presenter;
    @BindView(R.id.textViewTitleDetails)
    TextView title;
    @BindView(R.id.textViewDescriptionDetails)
    TextView description;
    @BindView(R.id.imageViewDetails)
    ImageView image;
    String urlArticleLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        setContentView(R.layout.activity_article_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        presenter = new ArticleDetailsPresenterImpl();
        presenter.setView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        presenter.setArticle(intent.getSerializableExtra(ArticlesListActivity.ARTICLE));
    }

    @Override
    public void setTitle(String title) {
        this.title.setText(title);
    }

    @Override
    public void setDescription(String description) {
        this.description.setText(description);
    }

    @Override
    public void setImage(String imageURL) {
        Picasso.with(getBaseContext()).load(imageURL).into(this.image);
    }

    @Override
    public void setButtonURL(String urlArticleLink) {
        this.urlArticleLink = urlArticleLink;
    }

    @OnClick(R.id.buttonWeb)
    public void viewArticleInBrowser() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(urlArticleLink));
        startActivity(intent);
    }

}
