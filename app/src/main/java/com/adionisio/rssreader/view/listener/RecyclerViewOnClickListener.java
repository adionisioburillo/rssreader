package com.adionisio.rssreader.view.listener;

import android.view.View;

/**
 * Created by Alejandro on 30/07/2018.
 */

public interface RecyclerViewOnClickListener {
    void onClick(View view, int position);
}
