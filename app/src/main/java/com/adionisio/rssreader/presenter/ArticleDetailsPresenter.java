package com.adionisio.rssreader.presenter;

import java.io.Serializable;

/**
 * Created by Alejandro on 30/07/2018.
 */

public interface ArticleDetailsPresenter {

    void setView(View view);

    void setArticle(Serializable article);

    interface View {
        void setTitle(String title);

        void setDescription(String description);

        void setImage(String imageURL);

        void setButtonURL(String urlArticleLink);

    }
}
