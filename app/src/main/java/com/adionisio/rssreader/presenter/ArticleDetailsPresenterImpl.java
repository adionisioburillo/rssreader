package com.adionisio.rssreader.presenter;

import com.adionisio.rssreader.model.Article;

import java.io.Serializable;

/**
 * Created by Alejandro on 30/07/2018.
 */

public class ArticleDetailsPresenterImpl implements ArticleDetailsPresenter {

    private Article article;
    private View view;

    @Override
    public void setView(View view) {
        this.view = view;
    }

    @Override
    public void setArticle(Serializable article) {
        this.article = (Article) article;
        view.setTitle(this.article.getTitle());
        view.setDescription(this.article.getDescription());
        view.setImage(this.article.getUrlToImage());
        view.setButtonURL(this.article.getUrl());
    }
}
