package com.adionisio.rssreader.presenter;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;

import com.adionisio.rssreader.api.RssServiceImpl;
import com.adionisio.rssreader.model.Article;
import com.adionisio.rssreader.model.RssResponse;
import com.adionisio.rssreader.utils.Comparators;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;

/**
 * Created by Alejandro on 30/07/2018.
 */

public class ArticlesListActivityPresenterImpl implements ArticlesListActivityPresenter {
    private View view;
    private Realm mRealm;

    @Override
    public void init(String source) {
        view.showLoading();
        Realm.init(view.getContext());
        mRealm = Realm.getDefaultInstance();
        getRssArticlesFromService(source);
    }


    private void getRssArticlesFromService(String source) {

        RssServiceImpl rssServiceImpl = new RssServiceImpl();
        rssServiceImpl.getRssArticles(source).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<RssResponse>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onNext(RssResponse rssResponse) {
                rssResponse.getArticles().sort(Comparators.comparatorArticles);
                view.showArticles(rssResponse.getArticles());
                removeArticlesFromDatabase();
                saveArticlesInDatabase(rssResponse.getArticles());
                view.hideLoading();
                view.sendFeedback("Articles updated");
            }

            @Override
            public void onError(Throwable e) {
                view.sendFeedback("Internet connection not available, last articles loaded");
                loadArticlesFromDatabase();
            }

            @Override
            public void onComplete() {
            }
        });

    }

    private void removeArticlesFromDatabase() {
        mRealm.beginTransaction();
        mRealm.delete(Article.class);
        mRealm.commitTransaction();
    }

    private void saveArticlesInDatabase(List<Article> articles) {
        for (Article article : articles) {
            mRealm.beginTransaction();
            Article articledb = mRealm.copyToRealm(article);
            mRealm.commitTransaction();
        }
    }

    private void loadArticlesFromDatabase() {
        if (mRealm.where(Article.class).count() > 0) {
            view.showArticles((ArrayList<Article>) mRealm.copyFromRealm(new ArrayList<Article>(mRealm.where(Article.class).findAll())));
            view.hideLoading();
        } else {
            view.sendFeedback("No articles found. Internet connection is required. Connect and launch RSSReader again.");
        }
    }

    @Override
    public void setView(View view) {
        this.view = view;
    }

    @Override
    public void finish() {
        mRealm.close();
    }

    @Override
    public void loadDataFromNewSource(String source) {
        view.sendFeedback("Loading " + source + " articles...");
        view.showLoading();
        getRssArticlesFromService(source);
    }

}
