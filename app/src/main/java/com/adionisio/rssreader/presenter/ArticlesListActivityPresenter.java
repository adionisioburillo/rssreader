package com.adionisio.rssreader.presenter;

import android.content.Context;

import com.adionisio.rssreader.model.Article;

import java.util.ArrayList;

/**
 * Created by Alejandro on 29/07/2018.
 */

public interface ArticlesListActivityPresenter {

    void setView(View view);

    void init(String source);

    void finish();

    void loadDataFromNewSource(String source);

    interface View {
        void showLoading();

        void hideLoading();

        void sendFeedback(String title);

        void showArticles(ArrayList<Article> articlesList);

        Context getContext();

    }
}
