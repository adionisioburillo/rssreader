package com.adionisio.rssreader.utils;

import com.adionisio.rssreader.model.Article;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Comparator;

/**
 * Created by Alejandro on 30/07/2018.
 */

public class Comparators {
    public static Comparator<Article> comparatorArticles = new Comparator<Article>() {

        @Override
        public int compare(Article o1, Article o2) {
            try {
                Calendar date1 = ISO8601.toCalendar(o1.getPublishedAt());
                Calendar date2 = ISO8601.toCalendar(o2.getPublishedAt());
                return date2.compareTo(date1);
            } catch (ParseException e) {
                return 0;
            }
        }
    };
}
