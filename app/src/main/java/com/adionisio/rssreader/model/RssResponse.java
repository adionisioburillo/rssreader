package com.adionisio.rssreader.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Alejandro on 29/07/2018.
 */

public class RssResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("totalResults")
    @Expose
    private Integer totalResults;
    @SerializedName("articles")
    @Expose
    private ArrayList<Article> articles = null;

    public ArrayList<Article> getArticles() {
        return articles;
    }
}
