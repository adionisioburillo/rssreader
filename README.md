# RSS-Reader
Android RSS reader

### Features

- Articles list sorted by date
- Article details
- Web link
- Search by title
- Multiples articles sources

Demo YouTube link:
 > :movie_camera: [RSS demo](https://youtu.be/9dxk2T7FTlk)
